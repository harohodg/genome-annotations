#!/usr/bin/env bash

apt-get update
apt-get install -y python3-minimal
apt-get install -y ipython
apt-get install -y ipython-notebook
apt-get install -y python-biopython
apt-get install -y python-networkx
apt-get install -y python-nltk
apt-get install -y git
