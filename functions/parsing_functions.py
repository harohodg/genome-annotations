'''
Created on 2015-05-15

@author: Harold Hodgins
@email : hodg4890@mylaurier.ca
@version: 0.1.0

Modified:
2015-07-10
-Added program argument parser
'''
import re, bz2, argparse
GENE_PATTERN = '.*\|.*\|(\d+)'
GENE_DATA_PATTERN  = "(?P<Kind>.*?)\t" + "(?P<Gene>.*?)\t" + "(?P<GI>\d+)\t" + "(?P<Locus_tag>.*?)\t" 
GENE_DATA_PATTERN += "(?P<start_coords>\d+)\.\.(?P<end_coords>\d+)\t" + "(?P<Strand>.)\t" 
GENE_DATA_PATTERN += "(protein_id:(?P<protein_ID>.*?))*;GeneID:(?P<Gene_ID>.*?)\t" + 'product="(?P<annotations>.*)"'

def parse_genes( file_name ):
    '''
    Function to parse homolog files and return a list of dictionaries containing gene data
    Preconditions : 
    file_name : (string) the bz2 file containing the tab delimited data
    
    Postconditions :
    returns a list of dictionaries 
    '''
    data = []
    try:
        with bz2.BZ2File( file_name ) as f:
            f.readline()
            for row in f:
                m = re.match(GENE_DATA_PATTERN, row.strip(),re.IGNORECASE|re.DOTALL)
                if m:
                    #print("We found {0} in \n{1}".format(m.groups(),row))
                    #print(m.groupdict())
                    data.append( m.groupdict() )
                else:
                    print("Unable to parse {0}".format(row))
    except EOFError, e:
        print(e)
    return data


def parse_homologs( file_name ):
    '''
    Function to parse homolog files and return a list of dictionaries containing homolog pairs
    Preconditions : 
    file_name : (string) the bz2 file containing the tab delimited data
    
    Postconditions :
    returns a list of dictionaries 
        keys : homolog_1 is the gene of interest (as int)
            homolog_2 is the homolog (as int)
    '''
    data = []
    try:
        with bz2.BZ2File( file_name ) as f:
            f.readline()
            for row in f:
                chunks = row.split("\t")
                try:
                    num_homologs = int(chunks[1])
                    m = re.search(GENE_PATTERN, chunks[0])
                    gene = int(m.group(1)) if m else None
                    if gene is None:
                        raise ValueError('Unable to parse gene {0}'.format(chunks[0]))
                    
                    homologs = []
                    for index in range(2, 2+num_homologs-1 ):
                        m = re.search(GENE_PATTERN, chunks[index])
                        homolog = int(m.group(1)) if m else None
                        if homolog is None:
                            raise ValueError('Unable to parse homolog {0}'.format(chunks[index]))
                        homologs.append( homolog )
                     
                    for homolog in homologs:
                        data.append( {"homolog_1":gene, "homolog_2":homolog} )
                except ValueError, e:
                    continue
                #print(e)
    except EOFError, e:
        print(e)
    return data
    

def args_parser():
    parser = argparse.ArgumentParser(description='Demo')
    parser.add_argument('gene_id',
                        type=int,
                        help='What gene identifier are we looking for' )
    
    parser.add_argument("-db","--database",
                        default = "genomics_data",
                        help = "What database are we using? (default = 'genomics_data')")
   
    parser.add_argument("-g","--graph",
                        default = False,
                        action='store_true',
                        help = "Are we creating a json graph?" ) 
    
    parser.add_argument("-f","--filters",
                        nargs="*",
                        help = "What filters are we applying to the annotations?",
                        choices = ["lower_case","replace_punctuation"])
    
    parser.add_argument("-ft","--frequency_threshold",
                        type = int,
                        default = 100,
                        help = "????:)????? Not actually implemented")
                        
    parser.add_argument("-s","--save_data",
                        action = "store_true",
                        default = False,
                        help = "Use / save the annotations for faster recall" )
                        
    parser.add_argument("-sr","--show_raw",
                        action = "store_true",
                        default = False,
                        help = "Show the raw annotations ordered by frequency. " )
                        
    parser.add_argument("-st","--show_tokens",
                        action = "store_true",
                        default = False,
                        help = "Show the token pairs (post filtering)." )
                        
    parser.add_argument("-sf","--show_filtered",
                        action = "store_true",
                        default = False,
                        help = "Show the annotations after filtering." )
                                                                                  
    return parser
