'''
Created on 2015-06-15

@author: Harold Hodgins
@email : hodg4890@mylaurier.ca
@version: 0.1.0
'''
from __future__ import print_function, division
import nltk


def item_frequency( raw_list, return_sorted = False ):
    '''
    Function to return 
    
    Preconditions:
    raw_list : the list of items to count
    sorted : Do we sort the final list by order of freqency
    '''
    num_items = len(raw_list)
    fd = nltk.FreqDist( raw_list )
    freqs = [ (token, fd[token]*1/num_items ) for token in fd]
    if return_sorted:
        freqs.sort(key=lambda tup: tup[1], reverse=True)  
    return freqs    
    
def export_graph( graph, path = [], file_name = "links.json"):
    '''
    Function to export a networkx graph as a json object
    
    Preconditions
    graph : the graph to export
    path : the path to export [] by default
    '''
    edges_data = list( graph.edges_iter(data=True) )
    with open(file_name,"w") as f:
        print('{"links":[', file=f)
        for edge in edges_data[:-1]:
            print('\t{', end="", file=f)
            link_type = "path" if (edge[0],edge[1]) in path else edge[2]["direction"]
            print('"source": "{0}", "target": "{1}", "type": "{2}"'.format(edge[0],edge[1], link_type), end="",file=f)
            print("},",file=f)
        if len(edges_data) > 1:
            edge = edges_data[-1]
            link_type = "path" if (edge[0],edge[1]) in path else edge[2]["direction"]
            print("{",end="",file=f)
            print('"source": "{0}", "target": "{1}", "type": "{2}"'.format(edge[0],edge[1], link_type), end="",file=f)
            print("}",file=f)
        print("]}",file=f)
    
    return
