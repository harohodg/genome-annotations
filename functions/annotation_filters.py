'''
Created on 2015-06-16

@author: Harold Hodgins
@email : hodg4890@mylaurier.ca
@version: 0.1.0
'''
PUNCTUATION = ("/","-",",","(",")","[","]")

def lower_case( annotations ):
    new_annotations = []
    for item in annotations:
        new_annotations.append( item.lower() )
        
    return new_annotations

def replace_punctuation( annotations ):
    new_annotations = []
    for item in annotations:
        for p in PUNCTUATION:
            item = item.replace(p, " ")
        new_annotations.append( item )
        
    return new_annotations
    