'''
Created on 2015-05-15

@author: Harold Hodgins
@email : hodg4890@mylaurier.ca
@version: 0.1.0

Functions for creating, populating, and updating the genomic database
'''

#from progressbar import ProgressBar
import sqlite3, os, bz2
import parsing_functions as pf
import re, logging, pickle

import glob, os
from Bio import SeqIO

GI_EXPRESSION = re.compile(ur'[Gg][Ii]:(\d+)')
HOMOLOG_GI_EXPRESSION = re.compile(ur'.*?\|.*?\|(\d+)')

UID_EXPRESSION = "uid(\d+)"
log = logging.getLogger(__name__)




def load_database( file_name = "genomics_data"  ):
    '''
    Function to create a new database and fill in the tables
    Preconditions:
    file_name  (string)   : the file name. (default = genomics_data)
    
    Postconditions:
    connection : the database connection 
    cursor     : the database cursor
    '''
    
    
    log.info("Attempting to open {0} database file".format(file_name) )
    connection = sqlite3.connect( file_name )
    cursor     = connection.cursor()
       
    log.info("creating tables (genomes, genes, homologs) if they don't already exist")    
    #cursor.execute('''create table if not exists genomes
    #               (uid INT PRIMARY KEY)''')
    
    cursor.execute('''create table if not exists genes
                   ( 
                   GI INT PRIMARY KEY, 
                   anotations TEXT
                   )''')
    
    cursor.execute('''create table if not exists homologs
                   (homolog_1 INT, 
                   homolog_2 INT,
                   FOREIGN KEY(homolog_1) REFERENCES genes(GI),
                   FOREIGN KEY(homolog_2) REFERENCES genes(GI)
                   )''')
    
    return connection, cursor



def populate_database(homologs_folder, genes_folder, cursor):
    '''
    Function to populate the database with values
    Preconditions :
    homologs_folder (string) : where are the homolog files stored
    genes_folder    (string) : where are the genes files stored
    cursor : the database cursor
    
    Postconditions :
    returns nothing
    
    Based on: http://stackoverflow.com/questions/16963352/decompress-bz2-files
    '''
#     log.info("Getting list of homolog file names from {0}".format(homologs_folder))
#     homolog_files = get_file_names(homologs_folder)
#     
#     log.info("Getting list of gene file names from {0}".format(genes_folder))
#     gene_files = get_file_names(genes_folder)
#     organism_files = homolog_files+gene_files
    
#     log.info("Populating Database with organisms")
#     load_organisms(organism_files, cursor)
    
    log.info("Populating Database with genes from the {0} folder".format(genes_folder) )
    load_genes(genes_folder, cursor)
    
    log.info("Populating Database with homologs from the {0} folder".format(homologs_folder) )
    load_homologs(homologs_folder, cursor)                
    return


def get_file_names( folder ):
    '''
    Function to return a list of file names
    Preconditions:
    folder : the folder to walk through
    '''
    file_names = []
    
    files = os.listdir( folder )
    for file_name in files:
        if file_name.endswith(".bz2") and file_name.startswith("uid"):
            file_names.append( os.path.join(folder, file_name) )
    return file_names



def get_uid( string ):
    '''
    Function to extract a UID from a string
    Preconditions :
    string : the string of interest
    
    returns uid : the uid or None if not found
    '''
    matches = re.search( UID_EXPRESSION, string )
    uid = int( matches.group(1) ) if matches else None
    return uid

def load_organisms( files, cursor):
    '''
    Function to load the database organisms table
    Preconditions :
    files : the files of interest
    cursor : the database cursor
     
    Postconditions:
    returns nothing
    '''
    log.info("Loading Organisms")
    pbar = ProgressBar()

    for file_name in pbar(files):
        uid = get_uid( file_name )
        if uid:
            try:
                cursor.execute("INSERT into organisms VALUES (?)", (int(uid),))
            except sqlite3.IntegrityError, e:
                log.info("UID {0} already exists in table organisms".format(uid) )
        else:
            log.error("No UID found in {0}".format(file_name) )

            
        
               
def load_genes( folder, cursor):
    '''
    Function to load gene data into the database
    Preconditions :
    files : the files of interest
    cursor : the database cursor
     
    Postconditions:
    returns nothing

    '''
    log.info("Loading genes")
    pbar = ProgressBar()
    
    
    for file_name in pbar( glob.glob("{0}/*.gbk".format(folder) ) ):
        log.info("Parsing {0}".format(file_name) )
        try:
            with open( file_name , "rU") as f:
                try:
                    for seq_record in SeqIO.parse(f, "genbank"):
                        for feature in seq_record.features:
                            if feature.type == "CDS" and "db_xref" in feature.qualifiers.keys() and "GI" in "".join( feature.qualifiers["db_xref"] ):
                                cmd = "Insert into GENES  values (?,?)"
                                gi = int( re.search(GI_EXPRESSION, "".join( feature.qualifiers["db_xref"] ) ).groups()[0] )
                                annotations = feature.qualifiers["product"][0] if "product" in feature.qualifiers.keys() else ""
                                cursor.execute( cmd, (gi, annotations) )
                except ValueError,e:
                    log.error("Problem with file {0} \n {1}".format(file_name,e))
        except IOError,e:
            log.error("Unable to open {0}".format(file_name) )

                    
    
    
    
#     for file_name in pbar(files):
#         parsed_data = pf.parse_genes( file_name )
#         for row in parsed_data:
#                 uid = get_uid(file_name)
#                 try:
#                     command = """INSERT into genes VALUES (?,?,?,?,?,?,?,?,?,?,?)"""
#                     assert row["GI"].isdigit(), "Invalid GI {0}".format( row["GI"])
#                     assert row["start_coords"].isdigit() and row["end_coords"].isdigit(), "Invalid start or end coords {0}:{1}".format(row["start_coords"],row["end_coords"])
#                     assert row["Gene_ID"].isdigit(), "Invalid Gene_ID {0}".format(row["Gene_ID"])
#                     cursor.execute(command, (int(uid),
#                                              int(row["GI"]),
#                                              row["Kind"],
#                                              row["Gene"],
#                                              row["Locus_tag"],
#                                              int(row["start_coords"]),
#                                              int(row["end_coords"]),
#                                              row["Strand"],
#                                              row["protein_ID"],
#                                              int(row["Gene_ID"]),
#                                              row["annotations"]
#                                              )
#                                    )
#                 except sqlite3.IntegrityError, e:
#                     log.info("GI {0} already exists in table organisms".format(row["GI"]) )
#                 except AssertionError, e:
#                     log.error(e)
    return

def load_homologs( folder, cursor):
    '''
    Function to load homolog data into the database
    Preconditions :
    files : the files of interest
    cursor : the database cursor
     
    Postconditions:
    returns nothing

    '''
    log.info("Loading homologs")
    pbar = ProgressBar()
    
    for file_name in pbar( glob.glob("{0}/*.bz2".format(folder) ) ):
        log.info("Parsing {0}".format(file_name) )
        try:
            with bz2.BZ2File( file_name ) as f:
                for line in f:
                    homologs = re.findall(HOMOLOG_GI_EXPRESSION, line)
                    if len(homologs)>1:
                        homolog_1 = int( homologs[0] )
                        for homolog_2 in homologs[1:]:
                            cmd = "Insert into homologs values (?,?)"
                            cursor.execute(cmd, (homolog_1, int(homolog_2) ) )
        except EOFError, e:
            log.error("Problem with {0} \n {1}".format(file_name,e))
            
#     for file_name in pbar(files):
#         parsed_data = pf.parse_homologs( file_name )
#         for row in parsed_data:
#             command = """INSERT into homologs VALUES (?,?)"""
#             cursor.execute(command, (row["homolog_1"], row["homolog_2"] )   )          
#     return

def get_annotations(gene_id, cursor, filters=None, use_pickle = False):
    '''
    Function to return the annotations for a given gene id and it's homologs
    '''
    annotations = []
    if use_pickle and os.path.isfile("{0}.herring".format(gene_id) ):
        annotations = pickle.load( open( "{0}.herring".format(gene_id), "rb" ) ) 
    else:
        cmd = """ select genes.anotations from genes, homologs
              where genes.GI = homologs.homolog_2 and homologs.homolog_1 = ?"""
        cursor.execute(cmd,(gene_id,) )
        for item in cursor.fetchall():
            annotations.append( item[0] )
        
    if filters is not None:
        for filter in filters:
            annotations = filter(annotations)
    
    if use_pickle:
        pickle.dump( annotations, open("{0}.herring".format(gene_id), "wb") )    
    return annotations, set(annotations)
