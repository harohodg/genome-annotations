#!/usr/bin/env python

from __future__ import print_function, division
from functions import database_functions as dbf
from functions.parsing_functions import args_parser
from functions.annotation_functions import item_frequency, export_graph
from functions import annotation_filters
import networkx as nx
from networkx import has_path
import sys
import nltk
import pickle

FILTERS = {"lower_case":annotation_filters.lower_case,
           "replace_punctuation":annotation_filters.replace_punctuation}

GRAPH_FILE_NAME = "./link_data.json"


parser = args_parser()
args = parser.parse_args()

gene_id = args.gene_id

conn, cur = dbf.load_database( file_name=args.database )

raw_annotations, unique_annotations = dbf.get_annotations(gene_id, cur, use_pickle= args.save_data)


l=len(raw_annotations)
print("We found {0:.0f} annotations for Gene ID {1}, {2} ({3:.2%}) were unique".format( l, gene_id, len(unique_annotations), len(unique_annotations)/l ) )

if args.show_raw:
    annotation_frequencies = item_frequency(raw_annotations, return_sorted=True)
    print("Here are the unique annotations sorted by how often they are in the raw list")
    for annotation in annotation_frequencies:
        print( "{0} - {1:.2%}".format(annotation[0],annotation[1]) )



if args.filters is not None:
    filters = args.filters if args.filters != [] else FILTERS.keys()
    filtered_annotations = list(unique_annotations)
    print("Applying the following {0} filter(s) : ".format(len(filters)))
    for filter_name in filters:
        print(filter_name)
        filtered_annotations = FILTERS[filter_name]( filtered_annotations )  
    unique_filtered_annotations = set(filtered_annotations) 
    u = len(unique_filtered_annotations)
    print("After filtering we are left with {0} unique annotations. {1:.2%} of the original list, {2:.2%} of the original unique list".format( u, u/l, u/len(unique_annotations) ) )
    
    if args.show_filtered:
        annotation_frequencies = item_frequency(filtered_annotations, return_sorted=True)
        print("Here are the unique filtered annotations sorted by how often they are in the raw list")
        for annotation in annotation_frequencies:
            print( "{0} - {1:.2%}".format(annotation[0],annotation[1]) )
else:
    unique_filtered_annotations = unique_annotations



raw_tokens = []
token_pairs = []
for annotation in unique_filtered_annotations:
    tokens  = [w for w in nltk.word_tokenize(annotation) if len(w) > 4 ]
    raw_tokens += tokens
    token_pairs += [ (tokens[i],tokens[i+1]) for i in range(len(tokens)-1) ]

pair_frequencies = item_frequency( token_pairs, return_sorted=True)
print("We found {0} token pairs (post filtering by length) of which {1:.2%} were unique".format(len(token_pairs), len(pair_frequencies)*1.0/len(token_pairs) ) )
if args.show_tokens:
    print("Here are the unique token pairs sorted by how often they are in the raw list")
    for pair in pair_frequencies:
        print( "{0} - {1:.2%}".format(pair[0],pair[1]) )


if args.graph:
    graph = nx.DiGraph()   
    for word_pair in pair_frequencies:
        graph.add_edge( *word_pair[0], attr_dict = {"weight": 1.0/word_pair[1], "freq":word_pair[1], "direction":"forward" } )
    export_graph( graph, file_name = GRAPH_FILE_NAME )
    print("Graph exported to {0}. Open graph.html in the same folder to view.".format(GRAPH_FILE_NAME ) 
    
conn.close()    
